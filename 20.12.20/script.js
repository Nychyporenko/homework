const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
  }
];

function createList(array = [], propertiesArray = []) {
  const ulFragmet = document.createDocumentFragment();
  array.forEach((item) => {
    try{
      const missedFields = [];
      propertiesArray.forEach(property => {
        if(!item.hasOwnProperty(property)) {
          missedFields.push(property);
        }
      });
      if(missedFields.length !== 0) {
        throw new Error(`${JSON.stringify(item)} has no "${missedFields.toString().replace(",", ", ")}"`)
      }
      const li = document.createElement('li');
      li.textContent = `${JSON.stringify(item)}`
      ulFragmet.append(li);
    } catch(e) {
      console.dir(e.message);
    }
  })
  console.log(ulFragmet);
  const ul = document.createElement("ul");
  ul.append(ulFragmet)
  return ul;
}

const list = createList(books, ["name", "author", "price"])
root.append(list);
