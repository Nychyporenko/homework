const display = document.querySelector(".display input");
const keysWrapper = document.querySelector(".keys");
keysWrapper.addEventListener("click", onBtnClick);
document.addEventListener('keydown', onBtnClick)
let isEnterContinuing = true;
let operator;
let firstOperand;
let secondOperand;
let result;
let memory;
let isMrcDouble;

function onBtnClick(e) {
  console.log(e)
  if(e.type === "click") {
    btnText = e.target.value;
  } else if (e.type === "keydown") {
    btnText = e.key;
  }
	switch(btnText) {
		case "1": case "2":	case "3": case "4":	case "5": case "6":	case "7": case "8":	case "9": case "0":
			isMrcDouble = false;
			// читать числа
			if(isEnterContinuing === true) {
				display.value = display.value + btnText;
			}
			if(isEnterContinuing === false) {
				display.value = btnText;
				isEnterContinuing = true;
			}
			break;
		case "=": case "Enter":
			isMrcDouble = false;
			isEnterContinuing = false;
			secondOperand = +display.value;
			switch (operator) {
				case "+":
					result = firstOperand + secondOperand;
					break;
				case "-":
					result = firstOperand - secondOperand;
					break;
				case "*":
					result = firstOperand * secondOperand;
					break;
				case "/":
					result = firstOperand / secondOperand;
					break;
			}
			display.value = result;
			firstOperand = result;
			secondOperand = undefined;
			operator = undefined;
			break;
		case "+": case "-":	case "*": case "/":
			isMrcDouble = false;
			isEnterContinuing = false;
			if(!firstOperand) {
				firstOperand = +display.value;
			} else if(operator === undefined) {
				firstOperand = +display.value;
			} else {
				secondOperand = +display.value;
			}
			if(secondOperand) {
				switch (operator) {
					case "+":
						result = firstOperand + secondOperand;
						break;
					case "-":
						result = firstOperand - secondOperand;
						break;
					case "*":
						result = firstOperand * secondOperand;
						break;
					case "/":
						result = firstOperand / secondOperand;
						break;
				}
				display.value = result;
				firstOperand = result;
			}
			operator = btnText;
			break;
		case "m-": case "m+":
			isMrcDouble = false;
			memory = +display.value;
			const displayWrapper = document.querySelector(".display");
			const mSpan = document.createElement("span");
			mSpan.classList.add("m-span")
			mSpan.style.cssText = "position: absolute; top: 5px; left: 5px; z-index: 1;";
			mSpan.style.visibility = "visible";
			mSpan.textContent = "m";
			displayWrapper.prepend(mSpan);
			isEnterContinuing = false;
			break;
		case "mrc":
      if(memory) {
        display.value = memory;
      }
			if(isMrcDouble) {
				memory = undefined;
				const mSpan = document.querySelector('.m-span');
				mSpan.style.visibility = "hidden";
			}
			isMrcDouble = true;
			break;
	}
}
