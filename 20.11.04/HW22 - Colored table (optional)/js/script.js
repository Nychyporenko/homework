const table = document.createElement('table');
const rowsFragment = document.createDocumentFragment();
const rowsArray = Array.from({length: 30}, () => document.createElement('tr'))
rowsArray.forEach(row => {
  cellsFragment = document.createDocumentFragment()
  const cellsArray = Array.from({length: 30}, () => document.createElement('td'))
  row.append(...cellsArray)
})
rowsFragment.append(...rowsArray)
table.append(rowsFragment)
document.body.append(table)

table.addEventListener('click', changeCellColor)

function changeCellColor(e) {
  e.target.classList.toggle("black")
}

document.body.addEventListener('click', reverseTableColors)

function reverseTableColors(e) {
  if(!e.target.closest('table')) {
    table.classList.toggle("color-reversed")
  }
}
