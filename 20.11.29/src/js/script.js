const menuBtn = document.querySelector(".navigation__button");
const mainMenu = document.querySelector(".main-menu");
const menuBtnBar = document.querySelectorAll(".menu-btn__bar");

const onMenuBtnClick = (e) => {
  Array.from(menuBtn.children).forEach(child => child.classList.toggle("open"));
  mainMenu.classList.toggle("open");
}

menuBtn.addEventListener("click", onMenuBtnClick);
