
/*--------------------------------------------------------------------------------------------------------------------------------------------------
Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в заданном диапазоне. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
Технические требования:
Считать с помощью модального окна браузера число, которое введет пользователь.
Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
*/

let num;
do {
  num = prompt("Enter a number");
} while (num === null || num.trim() === "" || isNaN(num));
if(num < 5) {
  console.log("Sorry, no numbers");
} else {
  for(let i = 0; i <= num; i++) {
    if(i % 5 === 0) {
      console.log(i);
    }
  }
}

/*--------------------------------------------------------------------------------------------------------------------------------------------------*/

/* Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять вывод окна на экран до тех пор, пока не будет введено целое число.
Считать два числа, m и n. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново. */

let n, m;
isNotAcceptable = true;
do {
  if(isNotAcceptable) {
    m = prompt("Enter integer number m");
    n = prompt("Ent1er integer number n");
  }
  if(n !== null
    && n.trim() !== ""
    && !isNaN(n)
    && Number.isInteger(+n)
    && m !== null
    && m.trim() !== ""
    && !isNaN(m)
    && Number.isInteger(+m)
  ) {
    isNotAcceptable = false;
  } else {
    alert("Enter integer numbers");
  }
} while (isNotAcceptable)
let numM = +m
numN = +n;
for(let checkedNum = numM; checkedNum <= numN; checkedNum++){
  let isPrime = true;
  for(let i = 2; i < checkedNum; i++) {
    if(checkedNum % i === 0) {
      isPrime = false;
    }
  }
  if(isPrime) {
    console.log(`${checkedNum} is prime ${checkedNum}`);
  }
}
