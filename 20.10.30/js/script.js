const outputList = (array, parentElement = document.body) => {
  const listItems = document.createDocumentFragment()
  const list = document.createElement('ul')
  for(item of array) {
    const listItem = document.createElement('li')
    if(Array.isArray(item)) {
      outputList(item, listItem)
    } else {
      listItem.textContent = item
    }
    listItems.append(listItem)
  }
  list.append(listItems)
  parentElement.appendChild(list)
}

// outputList(["a", ["b", "c"]])
