import {state, publish} from "../state/state.js";
import Form from "../classes/Form.js";
import Input from "../classes/Input.js";
import {sendCreateVisitRequest} from "../queries/sendCreateVisitRequest.js";
import {createVisitBtnObj, createVisitBtnElement} from "./createVisitBtnObj.js";
import Select from "../classes/Select.js";

export const createVisitFormObj = new Form();
export const createVisitFormElement = createVisitFormObj.element;

const SelectDoctorObj = new Select({values: ["Кардиолог", "Стоматолог", "Терапевт"]});
SelectDoctorObj.render(createVisitFormElement, "beforeend");

const InputAimObj = new Input({
  placeholder: "цель визита",
  name: "visitAim",
  type: "text",
  value: ""
});
InputAimObj.render(createVisitFormElement, "beforeend")
const InputAimElement = InputAimObj.element;

createVisitFormElement.addEventListener("submit", (e) => {
  e.preventDefault();



  const tokenPromise = sendLoginRequest({
    visitAim: InputAimElement.value,
    password: InputPasswordElement.value
  });
  tokenPromise
    .then(token => {
      if(token === "Incorrect username or password") {
        state.token = false;
      } else {
        state.token = token;
        publish({event: "loggedIn"})
      }
    })
})

// const InputPasswordObj = new Input({
//   placeholder: "password",
//   name: "password",
//   type: "password",
//   value: ""
// });
// InputPasswordObj.render(signUpFormElement, "beforeend");
// const InputPasswordElement = InputPasswordObj.element;

// const InputSubmitObj = new Input({
//   type: "submit",
//   value: "Login"
// });
// const InputSubmitElement = InputSubmitObj.element;
// InputSubmitObj.render(signUpFormElement, "beforeend");

// signUpFormElement.addEventListener("submit", (e) => {
//   e.preventDefault();
//   const tokenPromise = sendLoginRequest({
//     email: InputEmailElement.value,
//     password: InputPasswordElement.value
//   });
//   tokenPromise
//     .then(token => {
//       if(token === "Incorrect username or password") {
//         state.token = false;
//       } else {
//         state.token = token;
//         publish({event: "loggedIn"})
//       }
//     })
// })
