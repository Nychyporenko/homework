import {state, subscribe} from "../state/state.js";
import Button from "../classes/Button.js";

export const signUpBtnObj = new Button({
  text: "Вход"
})

export const signUpBtnElement = signUpBtnObj.element;

if(state.token) {
  signUpBtnElement.style.display = "none";
} else {
  signUpBtnElement.style.display = "float";
}

subscribe({
  event: "loggedIn",
  callback: () => {
    signUpBtnElement.style.display = "none";
  }
})
