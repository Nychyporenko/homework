import {state, subscribe} from "../state/state.js";
import Button from "../classes/Button.js";

export const createBtnObj = new Button({
  text: "Создать"
})
export const createBtnElement = createBtnObj.element;

if(state.token) {
  createBtnElement.style.display = "float";
} else {
  createBtnElement.style.display = "none";
}

subscribe({
  event: "loggedIn",
  callback: () => {
    createBtnElement.style.display = "flex";
  }
})
