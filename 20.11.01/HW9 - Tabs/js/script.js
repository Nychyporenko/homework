const tabsTitleEls = document.querySelectorAll(".tabs-title");
const tabsContentEls = document.querySelectorAll(".tabs-content li")
const tabsList = document.querySelector(".tabs");
const tabsContentList = document.querySelector(".tabs-content")
tabsList.addEventListener('click', onTabClick)

tabsContentEls.forEach(tabsContentEl => tabsContentEl.style.display = "none")
tabsContentEls[0].style.display = "block"

function onTabClick(e) {
	const targetTab = e.target;
	tabsTitleEls.forEach(tabsTitleEl => tabsTitleEl.classList.remove("active"))
	targetTab.classList.add("active");
	const tabsArray = Array.from(tabsList.children);
	const tabIndex = tabsArray.indexOf(targetTab)
	const visibleContentItem = document.querySelector(`.tabs-content li:nth-child(${tabIndex + 1})`)
	console.log(visibleContentItem)
	tabsContentEls.forEach(tabsContentEl => tabsContentEl.style.display = "none")
	visibleContentItem.style.display = "block";
}
