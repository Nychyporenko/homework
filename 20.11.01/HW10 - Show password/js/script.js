const iconsPassword = document.querySelectorAll(".icon-password");
iconsPassword.forEach(iconPassword => iconPassword.addEventListener('click', togglePasswordVisibilityHandler))

const btn = document.querySelector('.btn')
btn.addEventListener('click', onSubmit)

const alertSpan = document.createElement('span')
alertSpan.style.cssText = "color: red; font-size: 12px; margin-top: -20px; margin-bottom: 5px;";
alertSpan.textContent = "Нужно ввести одинаковые значения";

function togglePasswordVisibilityHandler(e) {
	const passwordIcon = e.target;
	if(passwordIcon.classList.contains("fa-eye")) {
		passwordIcon.classList.remove("fa-eye")
		passwordIcon.classList.add("fa-eye-slash")
		passwordIcon.closest(".input-wrapper").querySelector("input").type = "text"
	} else {
		passwordIcon.classList.add("fa-eye")
		passwordIcon.classList.remove("fa-eye-slash")
		passwordIcon.closest(".input-wrapper").querySelector("input").type = "password"
	}
}

function onSubmit() {
	const passwordInputEls = document.querySelectorAll("input");
	passwordInputEls[1].after(alertSpan)
	if(passwordInputEls[0].value === passwordInputEls[1].value) {
		setTimeout(alertSpan.style.display = "none", 0)
		alert("You are welcome")
	} else {
		alertSpan.style.display = "block"
	}
}
