const form = document.createElement('form')
const label = document.createElement('label')
const inputField = document.createElement("input")
const currentPriceDiv = document.createElement('div')
const currentPriceSpan = document.createElement('span');
const closeBtn = document.createElement('button')
const negativePriceErrorP = document.createElement('p')


label.innerText = "Price"
label.style = "margin-right: 8px;"
closeBtn.textContent = "✕";
closeBtn.style = "margin-left: 8px;";
currentPriceDiv.style = "visibility: hidden; margin-bottom: 8px;"
negativePriceErrorP.innerText = "Please enter correct price";
negativePriceErrorP.style = "display: none;"

document.body.append(form)
form.append(currentPriceDiv)
form.append(label)
form.append(inputField)
currentPriceDiv.append(currentPriceSpan)
currentPriceDiv.append(closeBtn)
inputField.after(negativePriceErrorP)

inputField.addEventListener('focus', onFocusIn)
inputField.addEventListener('focusout', onFocusOut);
closeBtn.addEventListener('click', onButtonClicked);

function onFocusIn () {
  inputField.style = "outline: 1px solid green;"
}

function onFocusOut (e) {
  inputField.style = "outline: none; color: green;"
  const price = e.target.value;
  if(+price < 0) {
    inputField.style = "outline: 2px solid red; color: green;";
    negativePriceErrorP.style = "display: block;";
    currentPriceDiv.style = "visibility: hidden; margin-bottom: 8px;"
  } else {
    currentPriceSpan.innerText = `Текущая цена: $${price}`
    currentPriceDiv.style = "visibility: visible; margin-bottom: 8px;"
    negativePriceErrorP.style = "display: none"
  }
}

function onButtonClicked() {
  currentPriceDiv.style = "visibility: hidden; margin-bottom: 8px;";
  inputField.value = "";
}
