const btn = document.querySelector('button')
btn.addEventListener("click", onBtnClick)

const container = document.createElement('div')
container.addEventListener('click', deleteCircle)

let circlesArray = [];

function onBtnClick () {
	const circlesRadius = +prompt("Enter circles diameter")
	container.style.cssText = `display: grid; grid-template-columns: repeat(10, ${circlesRadius}px);`
	document.body.append(container)
	circlesArray = Array.from({length: 100}, () => {
		const circle = document.createElement('div');
		circle.style.cssText = `width: ${circlesRadius}px; height: ${circlesRadius}px; border-radius: 50%; background-color: rgb(${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)});`
		return circle;
	});
	console.log(`btn click fn ${circlesArray.length}`)
	drawCircles(circlesArray)
}

function deleteCircle(e) {
	circlesArray = circlesArray.filter(circle => e.target !== circle)
	console.log(`delete circle fn ${circlesArray.length}`)
	drawCircles(circlesArray)
}

function drawCircles(circlesArray) {
	console.log(`draw circles fn ${circlesArray.length}`)
	const circlesHTMLFragment = document.createDocumentFragment()
	circlesHTMLFragment.append(...circlesArray)
	container.innerHTML = "";
	container.append(circlesHTMLFragment)
}
