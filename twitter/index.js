import {PostsController} from "./controllers/PostsController.js";

const root = document.getElementById("root");
const postsController = new PostsController(root);
postsController.render();