import {getPosts} from "../services/getPosts.js";
import {getUsers} from "../services/getUsers.js";
import {deletePost} from "../services/deletePost.js";
import {editPost} from "../services/editPost.js";
import {createNewPost} from "../services/createNewPost.js";
import {postsView} from "../views/postsView.js";
import {postView} from "../views/postView.js";
import {createBtnView} from "../views/createBtnView.js";
import {createPostModalView} from "../views/createPostModalView.js";
import {editCardFormView} from "../views/editCardFormView.js";

export class PostsController {
	constructor(rootElement) {
		this.rootElement = rootElement;
		this.postElements = [];
		this.savedPost;
		this.updatedPost;
	}

	renderPosts = async () => {
		const spinnerHTML = "<p class='loader'>loading posts...</p>"
		const spinnerHtmlElement = new DOMParser().parseFromString(spinnerHTML, 'text/html').body.childNodes[0];
		this.rootElement.append(spinnerHtmlElement);
		try {
			this.posts = await getPosts();
			[this.postsListElement, this.postElements] = postsView({
				element: this.rootElement, 
				posts: this.posts, 
				deleteCard: this.deleteCard,
				showEditForm: this.showEditForm,
				postElementsHost: this.postElements,
			});
			console.log("POST ELEMENTS: ", this.postElements)
		} catch(err) {
			console.log(err.message);
		}
	}

	renderUsers = async (postElements = this.postElements) => {
		try {
			this.users = await getUsers();
			console.log(this.users);
			postElements.forEach(element => this.addUserData(element))
		} catch(err) {
			console.log(err.message)
		}
	}

	addUserData = (postElement) => {
		const userNameElement = postElement.querySelector(".user-name");
		const userId = userNameElement.dataset.userId;
		const user = this.users.filter(user => +user.id === +userId)[0]	;
		userNameElement.textContent = `${user.name}`;
		const userEmailElement = postElement.querySelector(".user-email");
		userEmailElement.textContent = `${user.email}`
	}

	deleteCard = async (e) => {
		const cardElement = e.target.closest(".card");
		const cardId = cardElement.dataset.cardId;
		try {
			e.target.disabled = true;
			await deletePost(cardId);
			cardElement.remove();
		} catch(err) {
			console.log(err.message);
			e.target.disabled = false;
		}
	}

	showEditForm = async (e) => {
		const postElement = e.target.closest(".card");
		console.log("POST element: ", postElement)
		const cardId = postElement.dataset.cardId;
		try {
			e.target.disabled = true;
			const card = e.target.closest(".card");
			const cardId = e.target.closest(".card").dataset.cardId;
			const chosenPost = this.posts.filter(post => post.id == cardId)[0];
			const userId = chosenPost.userId;
			const chosenUser = this.users.filter(user => user.id == userId)[0];
			card.innerHTML = "";
			const editCardFormElement = await editCardFormView({
				card: card, 
				post: chosenPost,
				user: chosenUser,
				updatePost: this.updatePost,
			});
			editCardFormElement.addEventListener("submit", (e) => {
				this.saveChanges({e, chosenPost, chosenUser, card})
			});
		} catch(err) {
			console.log(err.message);
			e.target.disabled = false;
		}
	}

	saveChanges = async ({e, chosenPost, chosenUser, card}) => {
		e.preventDefault();
		await this.updatePost({
			event: e, 
			postId: chosenPost.id, 
			userId: chosenUser.id, 
			card: card
		});
		this.posts = this.posts.map(post => post.id === this.updatedPost.id 
			? this.updatedPost 
			: post
		);
		const newCard = postView(this.updatedPost);
		const userNameElement = newCard.querySelector(".user-name");
		const userId = userNameElement.dataset.userId;
		const user = this.users.filter(user => +user.id === +userId)[0]	;
		userNameElement.textContent = `${user.name}`;
		const userEmailElement = newCard.querySelector(".user-email");
		userEmailElement.textContent = `${user.email}`
		card.replaceWith(newCard);
	}

	updatePost = async ({event, postId, userId, card}) => {
		const editFormData = new FormData(event.target);
		editFormData.append("id", `${postId}`);
		editFormData.append("userId", `${userId}`);
		this.updatedPost = await editPost(editFormData);
	}

	renderCreateBtn = (e) => {
		const createBtnElement = createBtnView({
			element: this.rootElement, 
			showCreatePostModal: this.showCreatePostModal
		});
	}

	renderCreatePostModal = (e) => {
		this.createPostModalElement = createPostModalView({
			element: this.rootElement,
			createPostHandler: this.createPostHandler,
			hideCreatePostModal: this.hideCreatePostModal,
		});
	}

	showCreatePostModal = (e) => {
		this.createPostModalElement.style.display = "grid";
	}

	hideCreatePostModal = (e) => {
		if(e.target.contains(this.createPostModalElement)) {
			console.log(e.target.querySelector("#postTitle"))
			e.target.querySelector("#postTitle").value = "";
			e.target.querySelector("#postBody").value = "";
			e.target.style.display = "none";
		};
	}

	createPostHandler = async (e) => {
		const postBody = new FormData(e.target);
		postBody.append("userId", 1);
		const newPostData = await createNewPost(postBody);
		this.posts.push(newPostData)
		const newPostElement = postView(newPostData);
		this.postsListElement.prepend(newPostElement);
		this.createPostModalElement.querySelector("#postTitle").value = "";
		this.createPostModalElement.querySelector("#postBody").value = "";
		this.createPostModalElement.style.display = "none";
		this.addUserData(newPostElement);
	}

	render = async () => {
		this.renderCreateBtn();
		this.renderCreatePostModal();
		await this.renderPosts();
		await this.renderUsers();
	}
}