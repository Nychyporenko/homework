export function editCardFormView({card, post}) {
	const formHTML = `
			<form class="p-3">
				<div class="mb-3">
			    	<label 
			    		for="postTitle" 
		    			class="form-label"
		    		>Заголовок поста</label>
			    	<input
			    		type="text" 
			    		class="form-control" 
			    		id="postTitle" 
			    		aria-describedby="input title"
			    		name="title"
			    		value="${post.title}"
		    		>
  					<label for="postBody">Текст поста</label>
		    		<textarea 
		    			class="form-control mb-3" 
		    			id="postBody"
		    			name="body"
	    			>${post.body}</textarea>
			    </div>
			  	<button type="submit" class="btn btn-primary">Save</button>
			</form>
		`;

	const formElement = new DOMParser().parseFromString(formHTML, 'text/html').body.childNodes[0];
	card.append(formElement);
	return formElement;
}