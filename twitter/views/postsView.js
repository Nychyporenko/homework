export function postsView ({element, posts, deleteCard, showEditForm, postElementsHost}) {
	const postElements = [];
	let postsListHTML = `<ul class="container row row-cols-1 row-cols-md-2 row-cols-xl-3 mx-auto"></ul>`;
	const postsListElement = new DOMParser().parseFromString(postsListHTML, 'text/html').body.childNodes[0]
	
	// add listeners to remove and edit post cards
	postsListElement.addEventListener("click", async (e) => {
		if(e.target.classList.contains("delete-btn")) {
			deleteCard(e);
		} else if(e.target.classList.contains("edit-btn")) {
			showEditForm(e);
		}
	})

	posts.forEach(post => {
		const postHTML = `
			<div class="card p-0 m-3" data-card-id="${post.id}">
			  <div class="card-header df">
			    <h6 class="card-subtitle mb-2 user-name" data-user-id="${post.userId}">loading user ...</h6>
			    <span class="card-subtitle mb-2 text-muted user-email">loading email ...</span>
			  </div>
			  <div class="card-body">
			  	<h5 class="card-title">${post.title}</h5>
			    <p class="card-text">${post.body}</p>
			  </div>
			  <div class="card-footer">
  			    <button type="button" class="btn btn-primary edit-btn">Edit</button>
			    <button type="button" class="btn btn-danger delete-btn">Delete</button>
			  </div>
			</div>
		`;
		const postElement = new DOMParser().parseFromString(postHTML, 'text/html').body.childNodes[0];
		postsListElement.append(postElement);
		postElements.push(postElement);
	})
	element.querySelector(".loader").remove();
	element.append(postsListElement);
	return [postsListElement, postElements];
}

