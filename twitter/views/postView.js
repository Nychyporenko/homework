export const postView = post => {
	const postHTML = `
		<div class="card p-0 m-3" data-card-id="${post.id}">
		  <div class="card-header df">
		    <h6 class="card-subtitle mb-2 user-name" data-user-id="${post.userId}">loading user ...</h6>
		    <span class="card-subtitle mb-2 text-muted user-email">loading email ...</span>
		  </div>
		  <div class="card-body">
		  	<h5 class="card-title">${post.title}</h5>
		    <p class="card-text">${post.body}</p>
		  </div>
		  <div class="card-footer">
			    <button type="button" class="btn btn-primary edit-btn">Edit</button>
		    <button type="button" class="btn btn-danger delete-btn">Delete</button>
		  </div>
		</div>
	`;
	return new DOMParser().parseFromString(postHTML, 'text/html').body.childNodes[0];
}