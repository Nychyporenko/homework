const filterCollection = (arrayToFilter, searchRequestString, isSearchExact, ...pathesStringsArray) => {

  const pathesArraysArray = pathesStringsArray.map(pathString => pathString.split("."));

  const searchTerms = searchRequestString.toLowerCase().split(" ");

  let filteredArray = [];

  for(objectToTest of arrayToFilter) {
    let textToTest = "";
    pathesArraysArray.forEach(pathesArray => {
      textToTest += pathesArray.reduce((innerElement, key) => {
        if(Array.isArray(innerElement)) {
          let result = "";
          for(let innerObj of innerElement) {
            let finalProperty = pathesArray.slice(-1)[0];
            result += innerObj[finalProperty] ? innerObj[finalProperty].concat(" ") : "";
          }
          return result;
        } else if (innerElement && innerElement[key]) {
          return innerElement[key];
        } else {
          return null;
        }
      }, objectToTest);
    })

    if(isSearchExact) {
      if(
        !filteredArray.includes(objectToTest)
        && searchTerms.every(searchTerm => textToTest.toLowerCase().includes(searchTerm))
      ) {
        filteredArray.push(objectToTest);
      }
    } else {
      if(
        !filteredArray.includes(objectToTest)
        && searchTerms.some(searchTerm => textToTest.toLowerCase().includes(searchTerm))
      ) {
        filteredArray.push(objectToTest);
      }
    }

  }

  return filteredArray;

}


/* let result = filterCollection(
  [
    {a: '1st', b: [{secondName: "Ovr", name: "Denis"}]},
    {a: '2nd', b: [{secondName: "Nyc", name: "olya"}]},
  ],
  'Olya Nyc',
  false,
  'b.name', 'b.secondName'
) */


/*let result = filterCollection(
  [
    {a: 'aValue', b: {secondName: "Nyc", name: "hello"}, c: {address: "Kyiv"}},
    {a: 'aValue', b: {secondName: "Nyc", name: "hello Olya"}}
  ],
 'Olya',
 false,
 'b.name', 'c.address'
);*/

/* console.log(result); */
