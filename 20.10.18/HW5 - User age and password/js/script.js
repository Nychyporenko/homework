let createNewUser = function () {
  const firstName = prompt("Enter your first name");
  const lastName = prompt("Enter your last name");
  let birthday;
  do {
    birthdayInput = prompt("Enter your birth day: dd.mm.yyyy");
    let [dd, mm, yyyy] = birthdayInput.split(".");
    birthday = new Date(`${yyyy}.${mm}.${dd}`);
   } while (isNaN(birthday.getTime()));
  const newUser = {
    setFirstName: function(firstName) {
      Object.defineProperty(this, 'firstName', {
        enumerable: true,
        configurable: true,
        writable: false,
        value: firstName
      });
    },
    setLastName: function(lastName) {
      Object.defineProperty(this, 'lastName', {
        enumerable: true,
        configurable: true,
        writable: false,
        value: lastName
      });
    },
    setBirthday: function(birthday) {
      Object.defineProperty(this, 'birthday', {
        enumerable: true,
        configurable: false,
        writable: false,
        value: birthday
      });
    },
    getLogin: function() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function() {
      const todayDate = new Date();
      if(todayDate.getMonth() >= this.birthday.getMonth()
        && todayDate.getDate() > this.birthday.getDate()
      ) {
        return todayDate.getFullYear() - this.birthday.getFullYear();
      } else {
        return todayDate.getFullYear() - this.birthday.getFullYear() - 1;
      }
    },
    getPassword: function() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase()+this.birthday.getFullYear();
    }
  };
  newUser.setFirstName(firstName);
  newUser.setLastName(lastName);
  newUser.setBirthday(birthday);
  return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
