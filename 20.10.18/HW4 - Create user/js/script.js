/* Технические требования:

Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.


Необязательное задание продвинутой сложности:

Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства. */

let createNewUser = function () {
  const firstName = prompt("Enter your first name");
  const lastName = prompt("Enter your last name");
  const newUser = {
    setFirstName: function(firstName) {
      Object.defineProperty(this, 'firstName', {
        enumerable: true,
        configurable: true,
        writable: false,
        value: firstName
      });
    },
    setLastName: function(lastName) {
      Object.defineProperty(this, 'lastName', {
        enumerable: true,
        configurable: true,
        writable: false,
        value: lastName
      });
    },
    getLogin: function() {
      // return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    }
  };
  newUser.setFirstName(firstName);
  newUser.setLastName(lastName);
  return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getLogin());
