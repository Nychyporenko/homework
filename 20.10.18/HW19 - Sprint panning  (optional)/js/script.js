/*
Задание
Реализовать функцию, которая позволит оценить, успеет ли команда разработчиков сдать проект до наступления дедлайна. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Функция на вход принимает три параметра:

массив из чисел, который представляет скорость работы разных членов команды. Количество элементов в массиве означает количество человек в команде. Каждый элемент означает сколько стори поинтов (условная оценка сложности выполнения задачи) может выполнить данный разработчик за 1 день.
массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить). Количество элементов в массиве означает количество задач в беклоге. Каждое число в массиве означает количество стори поинтов, необходимых для выполнения данной задачи.
дата дедлайна (объект типа Date).


После выполнения, функция должна посчитать, успеет ли команда разработчиков выполнить все задачи из беклога до наступления дедлайна (работа ведется начиная с сегодняшнего дня). Если да, вывести на экран сообщение: Все задачи будут успешно выполнены за ? дней до наступления дедлайна!. Подставить нужное число дней в текст. Если нет, вывести сообщение Команде разработчиков придется потратить дополнительно ? часов после дедлайна, чтобы выполнить все задачи в беклоге

Работа идет по 8 часов в день по будним дням
 */


const sprintEstimator = (team, backlog, finalDate) => {
  let daysAvailable = 0;
  let runningDate = new Date();
  do {
	if(runningDate.getDay() !== 0 && runningDate.getDay() !== 1) {
		daysAvailable++;
	}
	runningDate.setDate(runningDate.getDate() + 1);
  } while (runningDate < finalDate);
  console.log(daysAvailable);
  let teamStorypointsPerDay = 0;
  for(let teammate of team) {
    teamStorypointsPerDay += teammate;
  }
  let backlogStorypointsSum = 0;
  for(let task of backlog) {
    backlogStorypointsSum += task;
  }
  let daysNeeded = 0;
  do {
	backlogStorypointsSum = backlogStorypointsSum - teamStorypointsPerDay;
	daysNeeded++;
  } while (backlogStorypointsSum > 0);
  if(daysAvailable > daysNeeded) {
	  console.log(`Все задачи будут успешно выполнены за ${daysAvailable - daysNeeded} дней до наступления дедлайна!`)
  } else {
	  console.log(`Команде разработчиков придется потратить дополнительно ${(daysNeeded - daysAvailable) * 8} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
  }
}

let finalDate = new Date();
finalDate.setDate(finalDate.getDate() + 7);

console.log(sprintEstimator([1,2],[15,9],finalDate));
