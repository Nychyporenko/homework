/* Задание
Создать объект студент "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Создать пустой объект student, с полями name и lastName.
Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия. */

const student = {
  name: "",
  lastName: "",
  table: {}
}

const name = prompt("Enter your name");
const lastName = prompt("Enter your last name");

student.name = name;
student.lastName = lastName;

let subject, grade;
do {
  subject = prompt("Enter subject");
  if(subject !== null) {
    grade = +prompt("Enter your grade");
    student.table[subject] = grade;
  }
} while (subject !== null)

console.log(student);

let badGradesNum = 0;
for(let key in student.table) {
  if(student.table[key] < 4) {
    badGradesNum++;
  }
}

if(badGradesNum < 1) {
  console.log("Студент переведен на следующий курс.");
}

let averageGrade, sum = 0, numberOfGrades = 0;
for(let key in student.table) {
  sum = sum + student.table[key];
  numberOfGrades++;
}

averageGrade = sum / numberOfGrades;

if(averageGrade > 7) {
  console.log("Студенту назначена стипендия");
}
