/* Задание
Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор. */

let obj1 = {
	name: "Roman",
	family: {
		sister: "Vasilisa",
		sistersTable: [{a: 1}, {b: 1}, {c: 1}],
	},
	table: {
		grades: [1, 1, 1],
	}
}

console.log("obj1 before: ", JSON.parse(JSON.stringify(obj1)));

let copyObj = function(obj) {
	let newObj = {};
	for(let key in obj) {
		if(typeof obj[key] !== "object") {
			newObj[key] = obj[key];
		} else if(typeof obj[key] === "object" && Array.isArray(obj[key])){
			newObj[key] = copyObj(obj[key]);
			newObj[key] = Object.values(newObj[key]);
		} else {
			newObj[key] = copyObj(obj[key]);
		}
	}
	return newObj;
}

let obj2 = copyObj(obj1);

console.log("obj2 before: ", obj2);

obj2.family.sister = "Marina";
obj2.family.sistersTable[1].b = 2;
obj2.table.grades[1] = 3;


console.log("obj1 after: ", obj1);
console.log("obj2 after: ", obj2);
