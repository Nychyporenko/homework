# Обьясните своими словами, как вы понимаете асинхронность в Javascript
Асинхронность в javascript означает, что выполнение функции можно отложить на потом.
Есть 2 места где может находится функция: либо call stack либо callback queue.
Функции, находящиеся наверху в call stack выполняются, это то, над чем работает тот самый единственный поток javacript.
Функции в callback queue попадают помещаются туда api браузера для ожидания, пока не опустеет call stack.
Как только call stack станет пустым, тогда event loop посмотрит есть ли в callback queue какие-нибудь функции и переместит их в call stack, где они и исполнятся.
Есть еще job queue - это то же что и callback queue, только специально для промисов.
