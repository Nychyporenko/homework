const btn = document.querySelector(".ip-btn");
const ipifyApi = "https://api.ipify.org";
const ipApi = "http://ip-api.com";
btn.addEventListener("click", getIPInfo);

// async function getIPInfo() {
//   const IP = await fetch(`${ipifyApi}/?format=json`)
//     .then(response => response.json())
//     .then(ipObj => fetch(`${ipApi}/json/${ipObj.ip}`)
//     .then(ipInfoResponse => ipInfoResponse.json())
//     .then(ipInfo => {console.log(ipInfo);})
//   )
// }

async function getIPInfo() {
  const response = await fetch(`${ipifyApi}/?format=json`);
  const ipObj = await response.json();
  const ipInfoResponse = await fetch(`${ipApi}/json/${ipObj.ip}`);
  const ipInfo = await ipInfoResponse.json();
  const ipInfoEl = createIpInfoEl(ipInfo);
  btn.insertAdjacentElement("afterend", ipInfoEl);
}

function createIpInfoEl(ipInfo) {
  const ipInfoEl = document.createElement("ul");
  const fieldElements = [];
  for(let key in ipInfo) {
    const fieldEl = document.createElement("li");
    fieldEl.textContent = `${key}: ${ipInfo[key]}`;
    fieldElements.push(fieldEl);
  }
  ipInfoEl.append(...fieldElements);
  return ipInfoEl;
}
