const ApiUrl = "https://swapi.dev/api";
const ROOT = document.querySelector("#root");

class Film {
	constructor(film) {
		this.film = film;
		this.filmEl = document.createElement("li");
	}
	createFilmEl() {
		this.filmEl.classList.add("film-item")
		const filmIdEl = document.createElement("span");
		filmIdEl.textContent = `episode id: \n ${this.film.episode_id}`;
		filmIdEl.classList.add("film__id");
		const filmTitleEl = document.createElement("h2");
		filmTitleEl.textContent = `Title: \n ${this.film.title}`;
		filmTitleEl.classList.add("film__title");
		const filmOpeningEl = document.createElement("p");
		filmOpeningEl.textContent = `Open crawl: \n ${this.film.opening_crawl}`;
		filmOpeningEl.classList.add("film__opening");
		this.filmEl.append(filmIdEl, filmTitleEl, filmOpeningEl);
		return this.filmEl;
	}
	createCharactersEl(characters) {
		const charactersEl = document.createElement("p");
			const linkElements = [];
			characters.forEach(character => {
				const linkEl = document.createElement("a");
				linkEl.href = "#void";
				linkEl.classList.add("character-link")
				linkEl.textContent = character.name;
				linkElements.push(linkEl)
			})
			charactersEl.append(...linkElements);
			return charactersEl;
	}
}

class Spinner {
	constructor() {
		this.spinnerEl = document.createElement("div");
	}
	createSpinnerEl() {
		const spinnerImg = document.createElement("img");
		spinnerImg.src = "./spinner.gif"
		this.spinnerEl.append(spinnerImg);
		this.spinnerEl.style.display = "none";
		this.spinnerEl.classList.add("spinner")
		console.log("create spinner: ", this.spinnerEl)
		return this.spinnerEl;
	}
	showSpinnerEl() {
		this.spinnerEl.style.display = "flex";
	}
	hideSpinnerEl() {
		this.spinnerEl.style.display = "none";
	}
}

class ListEl {
	constructor() {
		this.list = document.createElement("ul");
		this.filmsInstancesArray
	}
	createListEl() {
		this.list.classList.add("films-list")
		return this.list;
	}
	append(items) {
		this.list.append(items);
	}
	createFilmsInstancesArray(filmsArray) {
		this.filmsInstancesArray = filmsArray.map(film => new Film(film));
		return this.filmsInstancesArray;
	}
	renderFilmsList() {
		const filmsList = new ListEl().createListEl();
		this.filmsInstancesArray.forEach(filmInstance => {
			const filmEl = filmInstance.createFilmEl();
			filmsList.append(filmEl)
		});
		ROOT.append(filmsList);
	}
}

class Character {
	constructor() {
		this.list = document.createElement("p");
	}
	createCharactersEl() {
		return this.list;
	}
	append(items) {
		this.list.append(items);
	}
}

async function fetchAPI (url) {
	const response = await fetch(url);
	return await response.json();
}

async function fetchFilms(url) {
	const filmsUrl = `${url}/films/`
	const {results: filmsArray} = await fetchAPI(filmsUrl);;
	return filmsArray;
}

async function getCharacters (film) {
	const charactersDataPromises = film.characters.map(url => fetch(url))
	const charactersResponse = await Promise.all(charactersDataPromises);
	const charactersPromises = charactersResponse.map(characterData => characterData.json())
	const characters = await Promise.all(charactersPromises);
	return characters;
}

async function showFilms(ApiUrl) {
	const filmsArray = await fetchFilms(ApiUrl);
	const filmsList = new ListEl();
	const filmsInstancesArray = filmsList.createFilmsInstancesArray(filmsArray);
	filmsList.renderFilmsList();

	filmsInstancesArray.forEach(
		async function(filmInstance) {
			const {film} = filmInstance;
			const filmTitleEl = filmInstance.filmEl.querySelector(".film__title");
			const spinner = new Spinner();
			const spinnerEl = spinner.createSpinnerEl()
			spinner.showSpinnerEl();
			filmTitleEl.insertAdjacentElement("afterend", spinnerEl);
			const characters = await getCharacters(film);
			spinner.hideSpinnerEl();
			const charactersEl = filmInstance.createCharactersEl(characters);
			filmTitleEl.insertAdjacentElement("afterend", charactersEl);
		}
	)
}

showFilms(ApiUrl);
