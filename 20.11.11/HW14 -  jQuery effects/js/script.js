$(document).ready(function() {
  $(".main-menu-link").on("click", function (e) {
    e.preventDefault();
    let href = $(this).attr("href");
    let offset = $(href).offset().top;
    $("html, body").animate({
      scrollTop: offset,
    }, 900)
  })
})

$(".hide-btn").click(function () {
  $("section.popular-posts").slideToggle("slow");
});


const $btnTop = $(".btn-top");
$btnTop.hide();
$(window).on("scroll", function () {
  if($(window).scrollTop() >= $(window).height()) {
    $btnTop.fadeIn();
  } else {
    $btnTop.fadeOut();
  }
})

$btnTop.on("click", function () {
  $("html,body").animate({scrollTop:0},900)
})
