/*
Задание
Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Считать с помощью модального окна браузера два числа.
Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
Создать функцию, в которую передать два значения и операцию.
Вывести в консоль результат выполнения функции. */

/* Необязательное задание продвинутой сложности:
После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация). */

let firstNumber, secondNumber;
do {
  if(typeof firstNumber === "undefined" || typeof secondNumber === "undefined") {
    firstNumber = prompt("Enter first number");
    secondNumber = prompt("Enter second number");
  } else {
    firstNumber = prompt("Enter first number", firstNumber);
    secondNumber = prompt("Enter second number", secondNumber);
  }
} while (
  firstNumber === null
  || isNaN(+firstNumber)
  || firstNumber.trim() === ""
  || secondNumber === null
  || isNaN(+secondNumber)
  || secondNumber.trim() === ""
)

let operation = prompt("Enter one of these operations: +, -, *, /");

const calculate = function(firstNumber, secondNumber, operation) {
  switch(operation) {
    case "+":
      return +firstNumber + +secondNumber;
    case "-":
      return +firstNumber - +secondNumber;
    case "*":
      return +firstNumber * +secondNumber;
    case "/":
      return +firstNumber / +secondNumber;
  }
}

console.log(calculate(firstNumber, secondNumber, operation));
