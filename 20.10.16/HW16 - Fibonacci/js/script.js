/* Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
Считать с помощью модального окна браузера число, которое введет пользователь (n).
С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2). */

let n = prompt("Enter n");

const fibonacci = function(F0, F1, n) {
  if(n > 1) {
    F0New = F1;
    F1New = F0 + F1;
    n--;
    return fibonacci(F0New, F1New, n);
  } else {
    return F0;
  }
}

console.log(fibonacci(0, 1, n));
