class Employee {
	constructor(name, age, salary) {
		this.name = name || "'name' is undefined in constructor";
		this.age = age || "'age' is undefined in constructor";
		this.salary = salary || "'salary' is undefined in constructor";
	}
	set name(value) {
		if(typeof(value) === "string" && value.length > 0) {
			this._name = value;
		}
	}
	get name() {
		if(this._name === undefined) {
			return "'_name' is undefined"
		} else {
			return this._name;
		}
	}
	set age(value) {
		this._age = value;
	}
	get age() {
		return this._age;
	}
	set salary(value) {
		this._salary = value;
	}
	get salary() {
		return this._salary;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary) {
		super(name, age, salary);
		this.lang = [];
	}
	set salary(value) {
		this._salary = value;
	}
	get salary() {
		return this._salary * 3;
	}
	set lang(value) {
		this._lang = value;
	}
	get lang() {
		return this._lang;
	}
}

employee_1 = new Programmer("Oleh");
employee_2 = new Programmer("Sasha", 28, 1);
employee_1.name = "";
employee_1.salary = 1;
employee_1.lang = ["js", "typescript", "java"]
console.log(employee_1)
console.log(employee_2)
