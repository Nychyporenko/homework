# Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

каждый объект в Javascript имеет свойство `__proto__`. Этот объект содержит ссылку на свойство `prototype` класса (или функции-конструктора) с помощью которого был создан.
Добавляя в свойство `prototype` методы и свойства можно расширять класс (или функцию-конструктор), а все объекты, которые будут созданы на основе этого класса (или функции-конструктора) получат к ним доступ.
При поиске какого-то метода или свойства в объекте, в случае если этот объект не содержит искомого свойства, мы по ссылке `__proto__` поднимаемся к `prototype` класса (или функции-конструктора) и ищем там, и так далее. Конечным пунктом является функция конструктор `Object`, `__proto__` которого есть `null`. То-есть все объекты имеют доступ к методам и свойствам класса `Object`
