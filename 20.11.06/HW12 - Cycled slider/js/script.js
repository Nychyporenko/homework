const images = document.querySelectorAll("img");
const imagesArrayLength = images.length;
let index = 0;
function showImage(){
  images.forEach((img, idx) => {
    if(idx !== index) {
      img.style.display = "none";
      img.classList.remove("fade-in")
    } else {
      img.style.display = "flex";
      img.classList.add("fade-in")
    }
    console.log(img)
  })
}

const stopBtn = document.createElement('button')
stopBtn.innerText = "Прекратить";
document.body.append(stopBtn);
stopBtn.addEventListener("click", stopSlide)
const startBtn = document.createElement('button');
startBtn.innerText = "Возбновить показ";
document.body.append(startBtn)
startBtn.addEventListener("click", startSlide)
const monitor = document.createElement('span')
monitor.style.cssText = "display: flex;"
document.body.append(monitor)

let intervalId;
function startSlide() {
  onStart()
  intervalId = setInterval(onStart, 3000);
  return intervalId;
}

function onStart() {
  let startTime = Date.now();
  const monitorHandler = setInterval(() => {
    monitor.innerText = `${3000 - Date.now() + startTime}`;
  })
  setTimeout(() => {clearInterval(monitorHandler)}, 3000);
  showImage();
  index = index < imagesArrayLength - 1 ? ++index : 0;
}
intervalId = startSlide();

function stopSlide() {
  clearInterval(intervalId)
}
