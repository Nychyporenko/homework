const lightThemeBtn = document.getElementById('light-theme-btn')
const darkThemeBtn = document.getElementById('dark-theme-btn')
lightThemeBtn.addEventListener('click', setLightTheme);
darkThemeBtn.addEventListener('click', setDarkTheme);

function setLightTheme() {
  applyLightTheme();
  localStorage.setItem('theme', 'light')
}

function setDarkTheme() {
  applyDarkTheme();
  localStorage.setItem('theme', 'dark')
}

if(localStorage.getItem('theme') === null) {
  setLightTheme();
} else if(localStorage.getItem('theme') === 'light') {
  applyLightTheme();
} else if(localStorage.getItem('theme') === 'dark') {
  applyDarkTheme();
}

function applyDarkTheme() {
  document.querySelector('body').style.backgroundColor = "black";
  document.querySelector('body').style.color = "white";
  document.querySelector('.logo').style.color = "white";
  document.querySelector('.banner-container').style.backgroundColor = "white";
  document.querySelector('.side-menu').style.borderColor = "white";
  document.querySelector('.side-menu').style.color = "lightyellow";
}

function applyLightTheme() {
  document.querySelector('body').style.backgroundColor = "white";
  document.querySelector('body').style.color = "black";
  document.querySelector('.logo').style.color = "black";
  document.querySelector('.banner-container').style.backgroundColor = "white";
  document.querySelector('.side-menu').style.borderColor = "black";
  document.querySelector('.side-menu').style.color = "black";
}
