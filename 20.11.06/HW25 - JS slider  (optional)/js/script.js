const listItems = document.querySelectorAll("li");
listItems.forEach((li, idx) => li.style.order = idx + 1)

const prevBtn = document.getElementById("prev-btn")
const nextBtn = document.getElementById("next-btn")
prevBtn.addEventListener("click", prevBtnHandler)
nextBtn.addEventListener("click", nextBtnHandler)

const slider = document.getElementById("slider");
slider.addEventListener('transitionend', onSliderTransition)



function nextBtnHandler() {
	slider.classList.add("slider-transition")
	slider.style.transform = "translateX(-200px)"
}

function prevBtnHandler() {
	slider.classList.add("slider-transition")
	slider.style.transform = "translateX(200px)"
}

function onSliderTransition(e) {
	console.log(e)
	if(e.target.style.cssText === "transform: translateX(-200px);") {
		changeOrderNext();
		slider.classList.remove("slider-transition");
		slider.style.transform = "translateX(0)" ;
	} else {
		changeOrderPrev();
		slider.classList.remove("slider-transition");
		slider.style.transform = "translateX(0)" ;
	}
}




function changeOrderNext() {
	listItems.forEach(listItem => {
		if(+listItem.style.order === 1) {
			listItem.style.order = listItems.length;
		} else {
			--listItem.style.order;
		}
  })
}

function changeOrderPrev() {
	listItems.forEach(listItem => {
		if(+listItem.style.order === listItems.length) {
			listItem.style.order = 1;
		} else {
			++listItem.style.order;
		}
  })
}
